import React from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';

// lazy import pages
const HomePage = React.lazy(() => import('../pages/MainPage'));

export const AppRoutes = () => {
  const location = useLocation();

  // routes data
  const public_routes = [
    { id: 0, path: '/', page_component: <HomePage /> },
  ];

  return (
    <Routes location={location} key={location.pathname} >
      {public_routes.map((route) => (
        <Route
          key={route.id}
          path={route.path}
          element={route.page_component}
        />
      ))}
    </Routes>
  );
};
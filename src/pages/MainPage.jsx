import { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllPokemons } from '../store/asyncAction/pokemon';
import Config from '../components/App/Config/Config';
import PokemonList from '../components/App/PokemonList/PokemonList';
import { useObserver } from '../hooks/useObserver';


const MainPage = () => {
  const dispatch = useDispatch();

  const allPokemons = useSelector((state) => state.pokemon.pokemon_list);
  // eslint-disable-next-line max-len
  const pokemon_sorted_list = useSelector((state) => state.sort.pokemon_sorted_list);
  const search_pokemon = useSelector((state) => state.search.search_pokemon);

  const loadMore = useSelector((state) => state.pokemon.load_more);
  const is_loading = useSelector((state) => state.pokemon.is_loading);

  const lastElement = useRef();


  // eslint-disable-next-line max-len
  useObserver(lastElement, (loadMore != null && !pokemon_sorted_list.length && !search_pokemon.length), is_loading, () => {
    dispatch(getAllPokemons(loadMore));
  });

  const pokemonList = () =>{
    if (search_pokemon.length && !pokemon_sorted_list.length) {
      return search_pokemon;
    } else if (!search_pokemon.length && pokemon_sorted_list.length) {
      return pokemon_sorted_list;
    } else {
      return allPokemons;
    }
  };

  return (
    <div className="container">
      <Config />
      <PokemonList pokemonList={pokemonList()} />
      <button ref={lastElement} className="load-more"></button>
    </div>
  );
};

export default MainPage;
import styles from './input.module.scss';
import useClasses from '../../../hooks/useClasses';
import { useEffect, useState } from 'react';
import remove_icon from '../../../assets/media/icons/remove_icon.svg';
import PropTypes from 'prop-types';
const Input = ({ placeholder, value, clean, error, ...props }) => {
  const [focus, setFocus] = useState(false);
  const [inputWrapperFocus, setInputWrapperFocus] = useState(false);

  const input_placeholder = useClasses({
    [styles.input__placeholder]: true,
    [styles.input__placeholderActive]: focus,
  });

  const cleanValue = () =>{
    clean();
    setFocus(false);
  };

  const onFocus = ()=>{
    if (!value) {
      setFocus(true);
    }
  };
  const onBlur =()=>{
    if (!value) {
      setFocus(false);
    }
  };

  useEffect(() => {
    if (value.length) {
      setFocus(true);
    }
    if (!value.length && !inputWrapperFocus) {
      setFocus(false);
    }
  }, [value]);

  return (
    <div
      className={styles.input}
      onBlur={() => {
        setInputWrapperFocus(false);
      }}
      onFocus={() => {
        setInputWrapperFocus(true);
      }}
    >
      <p className={input_placeholder} aria-label='input-placeholder'>
        {placeholder}
      </p>
      <input
        {...props}
        value={value}
        onFocus={onFocus}
        onBlur={onBlur}
        className={styles.input__field}
        type="text"
      />

      {
        error ? <p className={styles.error}>{error}</p> : null
      }

      {value ?
        <img onClick={cleanValue}
          className={styles.search__icon}
          src={remove_icon}
        /> :
        null}
    </div>
  );
};

Input.propTypes = {
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  clean: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
};

export default Input;
import { render, screen } from '@testing-library/react';
import Input from './Input';
import userEvent from '@testing-library/user-event';

const setFakeText = jest.fn();
const cleanText = jest.fn();

describe('Testing input for', () => {
  it('input render', () => {
    render(<Input
      placeholder='placeholder'
      onChange={setFakeText}
      value="value1"
      clean={cleanText}
    />);
  });

  it('input value change', () => {
    render(<Input
      placeholder='placeholder'
      onChange={setFakeText}
      value="value1"
      clean={cleanText}
    />);

    const input = screen.getByRole('textbox');
    userEvent.type(input, 'react');
    expect(setFakeText).toHaveBeenCalledTimes(5);
  });

  it('placeholder classes', () => {
    // initialization
    render(<Input
      placeholder='placeholder'
      onChange={setFakeText}
      value=""
      clean={cleanText}
    />);

    const input = screen.getByRole('textbox');
    const input_placeholder = screen.getByLabelText('input-placeholder');

    // before focus
    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).not.toHaveClass('input__placeholderActive');

    // focus
    userEvent.click(input);
    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).toHaveClass('input__placeholderActive');

    // blur
    userEvent.click(document.body);
    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).not.toHaveClass('input__placeholderActive');


    render(<Input
      placeholder='placeholder'
      onChange={setFakeText}
      value="value"
      clean={cleanText}
    />);


    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).not.toHaveClass('input__placeholderActive');

    // // print text
    // userEvent.type(input, '1234');
    // expect(input_placeholder).toHaveClass('active-placeholder');

    // // clean input and blur
    // userEvent.type(input, '');
    // userEvent.click(document.body);
    // expect(input_placeholder).not.toHaveClass('active-placeholder');

    // // focus and print text
    // userEvent.click(input);
    // userEvent.type(input, '1234');
    // expect(input_placeholder).toHaveClass('active-placeholder');
  });

  it('placeholder classes after paste value', () => {
    // initialization
    render(<Input
      placeholder='placeholder'
      onChange={setFakeText}
      value="value1"
      clean={cleanText}
    />);

    const input = screen.getByRole('textbox');
    const input_placeholder = screen.getByLabelText('input-placeholder');

    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).toHaveClass('input__placeholderActive');

    // blur
    userEvent.click(document.body);
    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).toHaveClass('input__placeholderActive');

    // focus
    userEvent.click(input);
    expect(input_placeholder).toHaveClass('input__placeholder');
    expect(input_placeholder).toHaveClass('input__placeholderActive');
  });
});
import styles from './dropdown.module.scss';
import React, { useState } from 'react';
import useClasses from '../../../hooks/useClasses';
import PropTypes from 'prop-types';


const Dropdown = ({ list, title, value, selectOptions }) => {
  // data
  const [visible, setVisible] = useState(false);

  // dynamic classes
  const selectClasses = useClasses({
    [styles.dropdown__list]: true,
    [styles.dropdown__listActive]: visible,
  });

  // functions
  const toggleDropdown = () => {
    if (visible) {
      closeDropdown();
    } else {
      openDropdown();
    }
  };
  const openDropdown = () => {
    setVisible(true);
  };
  const closeDropdown = () => {
    setVisible(false);
  };

  const setValue =(value)=>{
    closeDropdown();
    selectOptions(value);
  };

  return (
    <div
      aria-label="dropdown"
      className={styles.dropdown}
      tabIndex={0}
      onClick={toggleDropdown}
      onBlur={closeDropdown}
    >
      <p className={styles.dropdown__title} aria-label="dropdown-title">
        {value ? `${title} - ${value}` : `${title} - ${list[0].name}`}
      </p>
      <div className={selectClasses} aria-label="dropdown-list">
        {list.map((variant, index) => (
          <p
            aria-label="dropdown-item"
            key={variant.name + index} onClick={() => {
              setValue(variant.name);
            }} className={styles.dropdown__item} >
            {index + 1} -  {variant.name}
          </p>
        ))}
      </div>
    </div>
  );
};

Dropdown.propTypes ={
  list: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  selectOptions: PropTypes.func.isRequired,
};

export default Dropdown;

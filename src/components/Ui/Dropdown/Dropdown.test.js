// eslint-disable-next-line max-len
import { render, screen, within } from '@testing-library/react';
import Dropdown from './Dropdown';
import userEvent from '@testing-library/user-event';

const list = [
  { name: 'name_1' },
  { name: 'name_2' },
  { name: 'name_3' },
];

const setNewValue = jest.fn();

describe('Testing app-select', ()=>{
  it('app select renders', () =>{
    // eslint-disable-next-line max-len
    render(<Dropdown list={list} title='title' value="" selectOptions={setNewValue} />);
    const { queryByText } = within(screen.getByLabelText('dropdown-title'));

    expect(queryByText('title - name_1')).toBeTruthy();
  });

  it('dropdown toggle visible', () =>{
    // eslint-disable-next-line max-len
    render(<Dropdown list={list} title='title' value="" selectOptions={setNewValue} />);

    const dropdown = screen.getByLabelText('dropdown');
    const dropdown_list = screen.getByLabelText('dropdown-list');

    // check base dropdown class
    expect(dropdown_list).not.toHaveClass('dropdown__listActive');

    // click on select (open)
    userEvent.click(dropdown);
    expect(dropdown_list).toHaveClass('dropdown__listActive');

    // click on select second time (close)
    userEvent.click(dropdown);
    expect(dropdown_list).not.toHaveClass('dropdown__listActive');
  });

  it('dropdown item choose', () =>{
    // eslint-disable-next-line max-len
    render(<Dropdown list={list} title='title' value="" selectOptions={setNewValue} />);

    const dropdown = screen.getByLabelText('dropdown');
    const dropdown_list = screen.getByLabelText('dropdown-list');
    const listOfItems = screen.getAllByLabelText('dropdown-item');
    // const listOfItems = getAllByLabelText('dropdown-item');

    expect(dropdown_list).not.toHaveClass('dropdown__listActive');

    // click on select (open)
    userEvent.click(dropdown);
    userEvent.click(listOfItems[1]);
    expect(dropdown_list).not.toHaveClass('dropdown__listActive');
    expect(setNewValue).toHaveBeenCalledTimes(1);
  });
});
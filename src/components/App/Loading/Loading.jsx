import { useSelector } from 'react-redux';
import styles from './loading.module.scss';
import pokeball from '../../../assets/media/icons/pokeball1.png';
import useClasses from '../../../hooks/useClasses';

const Loading = () => {
  const loading = useSelector((state) => state.pokemon.is_loading);
  const loading_visible = useClasses({
    [styles.loading__wrapper]: true,
    [styles.loading__wrapper__visible]: loading,
  });

  return (
    <div className={loading_visible}>
      <div className={styles.spinner__box}>
        <img className={styles.spinner__img} src={pokeball} />
      </div>
    </div>
  );
};

export default Loading;


import styles from './config.module.scss';
import Search from '../Search/Search';
import Dropdown from '../../Ui/Dropdown/Dropdown';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
// eslint-disable-next-line max-len
import { getAllTypes, getPokemonsByType } from '../../../store/asyncAction/pokemon_sort';
// eslint-disable-next-line max-len
import PokemonDisplayType from '../../App/PokemonDisplayType/PokemonDisplayType';
// eslint-disable-next-line max-len
import { cleanSearchedPokemon } from '../../../store/asyncAction/pokemon_search';

const Config = () => {
  const dispatch = useDispatch();
  const sort_by = useSelector((state) => state.sort.sort_by);
  const types_list = useSelector((state) => state.sort.types_list);

  const choosePokemonType = (value) =>{
    if (sort_by != value) {
      dispatch(cleanSearchedPokemon());
      dispatch(getPokemonsByType(value));
    }
  };

  useEffect(() => {
    dispatch(getAllTypes());
  }, []);


  return (
    <div className={styles.config}>

      <h1 className={styles.pokemon__title}>Pokemons</h1>

      <div className={styles.sort__dropdown}>
        <Dropdown
          list={types_list}
          title='sort by'
          selectOptions={choosePokemonType}
          value={sort_by}
        />
      </div>

      <div className={styles.search__wrapper}>
        <Search />
      </div>

      <div className={styles.display__type} >
        <PokemonDisplayType />
      </div>
    </div>
  );
};

export default Config;
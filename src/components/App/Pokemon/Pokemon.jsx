import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import useClasses from '../../../hooks/useClasses';
import Modal from '../Modal/Modal';
import styles from './pokemon.module.scss';
import PropTypes from 'prop-types';

const Pokemon = ({ pokemon_data, image }) => {
  const display_type = useSelector((state) => state.pokemon.display_type);
  const [modalVisible, setModalVisible] = useState(false);

  const pokemon = useClasses({
    [styles.pokemon]: true,
    [styles.pokemon__grid]: display_type == 'grid',
    [styles.pokemon__list]: display_type == 'list',
  });


  const closeModal = () => {
    setModalVisible(false);
  };

  return (
    <>
      <div className={pokemon} onClick={() => {
        setModalVisible(true);
      }}>
        <div className={styles.pokemon__number}>
          <small>#0{pokemon_data.id}</small>
        </div>
        <img
          className={styles.pokemon__img}
          src={image}
          alt={pokemon_data.name}
        />
        <div className={styles.pokemon__detail}>
          <h3>{pokemon_data.name}</h3>


          <div className={styles.pokemon__types}>
            {pokemon_data.types.map((type, index)=>(
              <span className={styles.pokemon__type} key={index}>
                {type.type.name}
              </span>
            ))}
          </div>
        </div>
      </div>

      <Modal modalVisible={modalVisible} closeModal={closeModal}>
        <div className={styles.modal__wrapper}>

          <div className={styles.modal__info}>
            <h3 className={styles.modal__name}>{pokemon_data.name}</h3>
            <div className={styles.modal__imageWrapper}>
              <img
                className={styles.modal__img}
                src={image}
                alt={pokemon_data.name}
              />
            </div>
          </div>


          <h2 className={styles.modal__movesTitle}>moves: </h2>

          <div className={styles.modal__movesList}>
            {
              pokemon_data.moves.map((move, index) => (
                <span className={styles.modal__move} key={index}>
                  {move.move.name}
                </span>
              ))
            }
          </div>

          <h2 className={styles.modal__statsTitle}>stats:</h2>

          <div className={styles.modal__statsList} >
            {
              pokemon_data.stats.map((stat, index) => (
                <span className={styles.modal__stat} key={index}>
                  {stat.stat.name}
                </span>
              ))
            }
          </div>
        </div>
      </Modal>
    </>

  );
};

Pokemon.propTypes ={
  pokemon_data: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    types: PropTypes.array.isRequired,
    moves: PropTypes.array.isRequired,
    stats: PropTypes.array.isRequired,
  }),
  image: PropTypes.string,
};

export default Pokemon;
import { useSelector } from 'react-redux';
import useClasses from '../../../hooks/useClasses';
import Pokemon from '../Pokemon/Pokemon';
import styles from './pokemon-list.module.scss';
import PropTypes from 'prop-types';

const PokemonList = ({ pokemonList }) => {
  const display_type = useSelector((state) => state.pokemon.display_type);

  const pokemon_wrapper = useClasses({
    [styles.pokemon__card]: true,
    [styles.pokemon__card__grid]: display_type == 'grid',
    [styles.pokemon__card__list]: display_type == 'list',
  });

  return (
    <div className={styles.pokemon__container}>
      {pokemonList.map((pokemonStats, index) =>
        <div className={pokemon_wrapper} key={index}>
          <Pokemon
            pokemon_data={pokemonStats}
            image={pokemonStats.sprites.other.home.front_default}
          />
        </div>,
      )}
    </div>
  );
};

PokemonList.propTypes ={
  pokemonList: PropTypes.array.isRequired,
};

export default PokemonList;
import Input from '../../Ui/Input/Input';
import styles from './search.module.scss';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useDebounce from '../../../hooks/useDebounce';
// eslint-disable-next-line max-len
import { cleanSearchedPokemon, searchPokemon } from '../../../store/asyncAction/pokemon_search';
import { setSearchValue } from '../../../store/reducers/PokemonSearchReducer';

const Search = () =>{
  const dispatch = useDispatch();
  const search_error = useSelector((state) => state.search.search_error);
  const search_value = useSelector((state) => state.search.search_value);
  const search_pokemon = useSelector((state) => state.search.search_pokemon);
  const debouncedSearchPokemon= useDebounce(search_value.trimStart(), 300);

  const setValue = (value) =>{
    dispatch(setSearchValue(value.trimStart()));
  };
  const cleanValue = () =>{
    dispatch(cleanSearchedPokemon(search_pokemon.length));
  };

  const error = ()=>{
    if (!search_value && search_error && search_pokemon.length) {
      return '';
    } else if (!search_value && search_error && !search_pokemon.length) {
      return '';
    } else {
      return search_error;
    }
  };

  useEffect(() => {
    if (debouncedSearchPokemon) {
      dispatch(searchPokemon(search_value));
    }
  },
  [debouncedSearchPokemon],
  );

  return (
    <div className={styles.search__wrapper}>
      <Input
        placeholder='search pokemon' onChange={(e)=>{
          setValue(e.target.value);
        }}
        clean={cleanValue}
        value={search_value}
        error={error()}
      />
    </div>
  );
};

export default Search;
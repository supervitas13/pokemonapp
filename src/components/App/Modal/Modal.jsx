import React, { useEffect } from 'react';
import { CSSTransition } from 'react-transition-group';
import reactDom from 'react-dom';
import styles from './modal.module.scss';
import close_icon from '../../../assets/media/icons/remove_icon.svg';
import PropTypes from 'prop-types';


const Modal = ({ children, modalVisible, closeModal }) => {
  const close = () => {
    closeModal();
  };

  useEffect(() => {
    if (modalVisible) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  }, [modalVisible]);

  return reactDom.createPortal(
      <>
        <CSSTransition
          in={modalVisible}
          timeout={300}
          classNames="modal"
          unmountOnExit
          mountOnEnter
        >
          <div className={styles.modal__wrapper}>
            <div className={styles.modal__body}>
              {children}

              <div onClick={close} className={styles.close__wrapper} >
                <img className={styles.close__icon} src={close_icon} alt="" />
              </div>
            </div>
          </div>
        </CSSTransition>
      </>,
      document.getElementById('portal'),
  );
};

Modal.propTypes ={
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  modalVisible: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default Modal;
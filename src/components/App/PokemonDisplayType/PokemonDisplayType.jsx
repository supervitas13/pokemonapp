import { useDispatch, useSelector } from 'react-redux';
import useClasses from '../../../hooks/useClasses';
import { setPokemonDisplayType } from '../../../store/reducers/PokemonReducer';
import styles from './pokemon-display-type.module.scss';

const PokemonDisplayType =() =>{
  const display_type = useSelector((state) => state.pokemon.display_type);

  const display_types = useClasses({
    [styles.display__type]: true,
    [styles.active__grid]: display_type == 'grid',
    [styles.active__list]: display_type == 'list',

  });
  const dispatch = useDispatch();
  const setDisplayType = (type) =>{
    dispatch(setPokemonDisplayType(type));
  };

  return (
    <div className={display_types}>
      <span className={styles.grid__type} onClick={()=>{
        setDisplayType('grid');
      }}>Grid</span>
      <span>/</span>
      <span className={styles.list__type} onClick={()=>{
        setDisplayType('list');
      }}>List</span>
    </div>
  );
};

export default PokemonDisplayType;
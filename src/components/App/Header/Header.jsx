import styles from './header.module.scss';
import poke_logo from '../../../assets/media/icons/pokemon_logo.png';

const Header = () =>{
  return (
    <div className={styles.header}>
      <div className={styles.logo__wrapper}>
        <img src={poke_logo} className={styles.logo} />
      </div>
    </div>
  );
};

export default Header;
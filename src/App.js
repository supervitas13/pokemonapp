import { Suspense } from 'react';
import { AppRoutes } from './helpers/appRoutes';
import { Provider } from 'react-redux';
import { store } from './store';
import Header from './components/App/Header/Header';
import Loading from './components/App/Loading/Loading';

// eslint-disable-next-line require-jsdoc
function App() {
  return (
    <Provider store={store} >
      <Header />
      <Loading />
      <Suspense fallback={<div className='preloader-suspence'></div>}>
        <AppRoutes />
      </Suspense>
    </Provider>

  );
}
export default App;

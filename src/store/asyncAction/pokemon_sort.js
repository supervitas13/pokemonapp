import axios from 'axios';
import { setPokemonListLoading } from '../reducers/PokemonReducer';
// eslint-disable-next-line max-len
import { cleanSortedArray, setAllTypes, setSort, setSortedArray } from '../reducers/PokemonSortReducer';
import { resetPokemonsListByAllType } from './pokemon';

export const getPokemonsByType = (type) => {
  return async function(dispatch) {
    dispatch(setPokemonListLoading(true));
    dispatch(cleanSortedArray());
    dispatch(setSort(type));

    if (type != 'all') {
      const data = await axios.get((`https://pokeapi.co/api/v2/type/${type}`));
      await Promise.all(data.data.pokemon.map(async (pokemon) => {
        await dispatch(getOnePokemonInfo(pokemon.pokemon));
      }));
      console.log('sort'),
      dispatch(setPokemonListLoading(false));
    } else {
      dispatch(resetPokemonsListByAllType());
    }
  };
};
export const getAllTypes = () => {
  return async function(dispatch) {
    const data = await axios.get('https://pokeapi.co/api/v2/type');
    dispatch(setAllTypes(data.data.results));
  };
};
const getOnePokemonInfo = (pokemon) => {
  return async function(dispatch) {
    const data = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`);
    dispatch(setSortedArray(data.data));
  };
};

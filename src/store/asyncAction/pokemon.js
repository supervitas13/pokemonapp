import axios from 'axios';
// eslint-disable-next-line max-len
import { cleanPokemonList, setAllPokemons, setLoadMore, setPokemonListLoading } from '../reducers/PokemonReducer';

export const getAllPokemons = (loadMore) => {
  return async function(dispatch) {
    dispatch(setPokemonListLoading(true));
    const data = await axios.get(loadMore);
    dispatch(setLoadMore(data.data.next));

    await Promise.all(data.data.results.map(async (pokemon) => {
      await dispatch(getOnePokemonInfo(pokemon));
    }));

    console.log('all');

    dispatch(setPokemonListLoading(false));
  };
};

export const getOnePokemonInfo = (pokemon) => {
  return async function(dispatch) {
    const data = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`);
    dispatch(setAllPokemons(data.data));
  };
};

export const resetPokemonsListByAllType = () =>{
  return function(dispatch) {
    dispatch(cleanPokemonList());
    dispatch(setLoadMore('https://pokeapi.co/api/v2/pokemon?limit=20'));
    dispatch(getAllPokemons('https://pokeapi.co/api/v2/pokemon?limit=20'));
  };
};



import axios from 'axios';
// eslint-disable-next-line max-len
import { setSearchedPokemon, setSearchError, setSearchValue } from '../reducers/PokemonSearchReducer';
import { cleanSortedArray, setSort } from '../reducers/PokemonSortReducer';
import { resetPokemonsListByAllType } from './pokemon';

export const searchPokemon = (value) => {
  return async function(dispatch) {
    try {
      const data = await axios.get(`https://pokeapi.co/api/v2/pokemon/${value}`);
      dispatch(setSearchError(''));
      dispatch(setSearchedPokemon([data.data]));
      dispatch(cleanSortedArray());
      dispatch(setSort('all'));
    } catch (error) {
      dispatch(setSearchError('not found'));
      dispatch(setSearchedPokemon([]));
    }
  };
};

export const cleanSearchedPokemon = (result) => {
  return async function(dispatch) {
    dispatch(setSearchValue(''));
    dispatch(setSearchedPokemon([]));
    dispatch(setSearchError(''));

    if (result) {
      dispatch(resetPokemonsListByAllType());
    }
  };
};
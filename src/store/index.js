import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { applyMiddleware } from '@reduxjs/toolkit';

// reducer
import { PokemonReducer } from './reducers/PokemonReducer';
import { PokemonSortReducer } from './reducers/PokemonSortReducer';
import { PokemonSearchReducer } from './reducers/PokemonSearchReducer';

const rootReducer = combineReducers({
  pokemon: PokemonReducer,
  sort: PokemonSortReducer,
  search: PokemonSearchReducer,
});


export const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk)),
);
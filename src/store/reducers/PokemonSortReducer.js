const defaultState = {
  pokemon_sorted_list: [],
  sort_by: 'all',
  types_list: [
    {
      name: 'all',
      url: 'https://pokeapi.co/api/v2/pokemon?limit=20',
    }],
};

const SET_ALL_TYPES = 'SET_ALL_TYPES';
const SET_SORTED_ARRAY = 'SET_SORTED_ARRAY';
const SET_SORT = 'SET_SORT';
const CLEAN_SORTED_ARRAY = 'CLEAN_SORTED_ARRAY';

export const PokemonSortReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_ALL_TYPES:
      return { ...state, types_list: [...state.types_list, ...action.payload] };
    case SET_SORT:
      return { ...state, sort_by: action.payload };
    case SET_SORTED_ARRAY:
      // eslint-disable-next-line max-len
      return { ...state, pokemon_sorted_list: [...state.pokemon_sorted_list, action.payload] };
    case CLEAN_SORTED_ARRAY:
      return { ...state, pokemon_sorted_list: [] };
    default:
      return state;
  }
};

export const setAllTypes = (payload) => ({ type: SET_ALL_TYPES, payload });
export const setSort = (payload) => ({ type: SET_SORT, payload });
// eslint-disable-next-line max-len
export const setSortedArray = (payload) => ({ type: SET_SORTED_ARRAY, payload });
export const cleanSortedArray = () => ({ type: CLEAN_SORTED_ARRAY });
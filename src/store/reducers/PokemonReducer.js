const defaultState = {
  pokemon_list: [],
  is_loading: false,
  display_type: 'grid',
  load_more: 'https://pokeapi.co/api/v2/pokemon?limit=20',
};

const SET_ALL_POKEMONS = 'SET_ALL_POKEMONS';
const SET_POKEMONS_DISPLAY_TYPE = 'SET_POKEMONS_DISPLAY_TYPE';
const SET_LOAD_MORE = 'SET_LOAD_MORE';
const SET_POKEMONS_LIST_LOADING = 'SET_POKEMONS_LIST_LOADING';
const CLEAN_POKEMONS_LIST = 'CLEAN_POKEMONS_LIST';

export const PokemonReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_ALL_POKEMONS:
      // eslint-disable-next-line max-len
      return { ...state, pokemon_list: [...state.pokemon_list, action.payload] };
    case SET_LOAD_MORE:
      return { ...state, load_more: action.payload };
    case SET_POKEMONS_DISPLAY_TYPE:
      return { ...state, display_type: action.payload };
    case SET_POKEMONS_LIST_LOADING:
      return { ...state, is_loading: action.payload };
    case CLEAN_POKEMONS_LIST:
      return { ...state, pokemon_list: [] };
    default:
      return state;
  }
};

// eslint-disable-next-line max-len
export const setAllPokemons = (payload) => ({ type: SET_ALL_POKEMONS, payload });
export const setLoadMore = (payload) => ({ type: SET_LOAD_MORE, payload });
// eslint-disable-next-line max-len
export const setPokemonDisplayType = (payload) => ({ type: SET_POKEMONS_DISPLAY_TYPE, payload });
// eslint-disable-next-line max-len
export const setPokemonListLoading = (payload) => ({ type: SET_POKEMONS_LIST_LOADING, payload });
// eslint-disable-next-line max-len
export const cleanPokemonList = (payload) => ({ type: CLEAN_POKEMONS_LIST, payload });



const defaultState = {
  search_pokemon: [],
  search_value: '',
  search_error: '',
};

const SET_SEARCH_POKEMON = 'SET_SEARCH_POKEMON';
const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE';
const SET_SEARCH_POKEMON_ERROR = 'SET_SEARCH_POKEMON_ERROR';


export const PokemonSearchReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_SEARCH_POKEMON:
      return { ...state, search_pokemon: action.payload };
    case SET_SEARCH_VALUE:
      return { ...state, search_value: action.payload };
    case SET_SEARCH_POKEMON_ERROR:
      return { ...state, search_error: action.payload };
    default:
      return state;
  }
};

// eslint-disable-next-line max-len
export const setSearchedPokemon = (payload) => ({ type: SET_SEARCH_POKEMON, payload });
// eslint-disable-next-line max-len
export const setSearchValue = (payload) => ({ type: SET_SEARCH_VALUE, payload });
// eslint-disable-next-line max-len
export const setSearchError = (payload) => ({ type: SET_SEARCH_POKEMON_ERROR, payload });


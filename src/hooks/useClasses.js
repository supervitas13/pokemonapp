import { useEffect, useState } from 'react';

const useClasses = (classesForWatch = {}) => {
  const [classes, setClasses] = useState([]);

  const addClass = (class_name) => {
    if (!classIncludes(class_name)) {
      setClasses([...classes, class_name]);
    }
  };

  const removeClass = (class_name) => {
    if (classIncludes(class_name)) {
      setClasses(classes.filter((one_class) => one_class != class_name));
    }
  };

  const classIncludes = (class_name) => {
    return Boolean(classes.filter(
        (one_class) => one_class == class_name).length != 0,
    );
  };

  const checkClasses = () => {
    for (const key in classesForWatch) {
      if (classesForWatch[key]) {
        addClass(key);
      } else {
        removeClass(key);
      }
    }
  };

  useEffect(() => {
    checkClasses();
  }, [classesForWatch]);

  return [classes.join(' ')];
};

export default useClasses;